const formulario = document.getElementById("Formulario");
const inputs = document.querySelectorAll(".form-control");
const aceptarCheckbox = document.getElementById("aceptar-checkbox");

window.onload = function() {
    formulario.reset();
    campos.nombre = false;
    campos.apellido = false;
    campos.edad = false;
    campos.ciudad = false;
    campos.direccion = false;
    campos.email = false;

    inputs.forEach((input) => {
        input.parentElement.classList.remove("form-correct", "form-incorrect");
    });

    document.getElementById("formulario-mensaje").classList.remove("formulario-mensaje-active");
    document.getElementById("formulario-mensaje-enviado").classList.remove("formulario-mensaje-enviado-active");
};

const expresiones = {
    nombre: /^[a-zA-Z\s]+$/    ,
    apellido: /^[a-zA-Z\s]+$/,
    edad: /^\d+$/,
    ciudad: /^[a-zA-Z\s]+$/,
    direccion: /^[a-zA-Z0-9\s,'.-]+$/,
    email: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
};

const campos = {
    nombre: false,
    apellido: false,
    edad: false,
    ciudad: false,
    direccion: false,
    email: false
};

const validarCampo = (expresion, input, campo) => {
    if (expresion.test(input.value)) {
        document.getElementById(`grupo-${campo}`).classList.remove("form-incorrect");
        document.getElementById(`grupo-${campo}`).classList.add("form-correct");
        document.querySelector(`#grupo-${campo} i`).classList.add("fa-face-laugh");
        document.querySelector(`#grupo-${campo} i`).classList.remove("fa-solid", "fa-circle-xmark");
        document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.remove("formulario-input-error-active");
        campos[campo] = true;
    } else {
        document.getElementById(`grupo-${campo}`).classList.add("form-incorrect");
        document.getElementById(`grupo-${campo}`).classList.remove("form-correct");
        document.querySelector(`#grupo-${campo} i`).classList.remove("fa-solid", "fa-circle-xmark");
        document.querySelector(`#grupo-${campo} i`).classList.add("fa-face-laugh");
        document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.add("formulario-input-error-active");
        campos[campo] = false;
    }
};

const validarFormulario = (e) => {
    switch (e.target.name) {
        case "nombre":
            validarCampo(expresiones.nombre, e.target, "nombre");
            break;
        case "apellido":
            validarCampo(expresiones.apellido, e.target, "apellido");
            break;
        case "edad":
            validarCampo(expresiones.edad, e.target, "edad");
            break;
        case "ciudad":
            validarCampo(expresiones.ciudad, e.target, "ciudad");
            break;
        case "direccion":
            validarCampo(expresiones.direccion, e.target, "direccion");
            break;
        case "email":
            validarCampo(expresiones.email, e.target, "email");
            break;
    }
    
};

inputs.forEach((input) => {
    input.addEventListener("input", (e) => {
        validarFormulario(e); 
    });
});

formulario.addEventListener("submit", (e) => {
    e.preventDefault();

    if (campos.nombre && campos.apellido && campos.edad && campos.ciudad && campos.direccion && campos.email) {
        formulario.reset(); // Si todos los campos están completados correctamente, se resetea el formulario
        document.getElementById("formulario-mensaje-enviado").classList.add("formulario-mensaje-enviado-active");

        setTimeout(() => {
            document.getElementById("formulario-mensaje-enviado").classList.remove("formulario-mensaje-enviado-active");
        }, 5000); // Después de 5 segundos, desaparecerá el mensaje de envío exitoso

        document.getElementById("formulario-mensaje").classList.remove("formulario-mensaje-active"); // Oculta el mensaje de error si está visible
        
        // Restaurar el estado de los iconos de validación
        document.querySelectorAll(".formulario-validacion-estado").forEach((icono) => {
            icono.classList.remove("fa-circle-exclamation"); // Clase para el icono de alerta
            icono.classList.add("fa-circle-check"); // Clase para el icono de verificación
        });
    } else {
        document.getElementById("formulario-mensaje").classList.add("formulario-mensaje-active"); // Muestra el mensaje de error si no todos los campos están completados correctamente
    }
});
