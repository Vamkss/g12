const apiUrl = 'http://localhost:3000/products';

const createProduct = async () => {
    const name = document.getElementById('productName').value;
    const price = document.getElementById('productPrice').value;
    const category = document.getElementById('productCategory').value;
    const stock = document.getElementById('productStock').value;
    const image = document.getElementById('productImage').value;

    const product = { name, price, category, stock, image };

    try {
        const response = await fetch(`${apiUrl}/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product)
        });
        const result = await response.json();
        console.log(result);
        getAllProducts(); // Refresh the product list after creating a new product
        alert('Producto creado!'); // Show alert after product creation
        window.location.href = './index.html'
    } catch (error) {
        console.error('Error:', error);
    }
};

const getAllProducts = async () => {
    try {
        const response = await fetch(apiUrl);
        const products = await response.json();
        const tableBody = document.getElementById('productTableBody');
        tableBody.innerHTML = ''; // Clear existing table rows

        products.forEach(product => {
            const row = document.createElement('tr');
            row.innerHTML = `
                <td>${product.id}</td>
                <td>${product.name}</td>
                <td>${product.price}</td>
                <td>${product.category}</td>
                <td>${product.stock}</td>
                <td><img width="60" src="${product.image}" alt="${product.name}"></td>
                <td>
                    <a class="btn btn-primary" href="./update_product.html?id=${product.id}">Actualizar</a>
                    <button class="btn btn-danger" onclick="deleteProduct(${product.id})">Eliminar</button>
                </td>
            `;
            tableBody.appendChild(row);
        });
    } catch (error) {
        console.error('Error:', error);
    }
};

const updateProduct = async () => {
    const productId = new URLSearchParams(window.location.search).get('id'); // Get product id from URL query parameter

    const newName = document.getElementById('productName').value;
    const newPrice = document.getElementById('productPrice').value;
    const newCategory = document.getElementById('productCategory').value;
    const newStock = document.getElementById('productStock').value;
    const newImage = document.getElementById('productImage').value;

    const updatedProduct = { name: newName, price: newPrice, category: newCategory, stock: newStock, image: newImage };

    try {
        const response = await fetch(`http://localhost:3000/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedProduct)
        });
        const result = await response.json();
        console.log(result);
        alert('Producto actualizado correctamente.'); // Show alert after product update
        // Optionally, redirect to index.html after update
        window.location.href = './index.html';
    } catch (error) {
        console.error('Error:', error);
    }
};


const deleteProduct = async (id) => {
    // Show confirmation dialog before deleting
    if (confirm("Esta seguro que quiere eliminar este producto?")) {
        try {
            const response = await fetch(`${apiUrl}/${id}`, {
                method: 'DELETE'
            });
            const result = await response.json();
            console.log(result);
            getAllProducts(); // Refresh the product list after deleting a product
            alert('Producto eliminado de forma exitosa.'); // Show alert after product update
        } catch (error) {
            console.error('Error:', error);
        }
    }
};

