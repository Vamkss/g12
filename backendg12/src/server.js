// Importamos las dependencias necesarias
const express = require('express');
const path = require('path');
const cors = require('cors');

// Instanciamos express
const app = express();

// Importamos las rutas
const productRoute = require('../routes/productRoutes'); // Ajusta la ruta si es necesario
const authRoutes = require('../routes/authRoutes'); // Ajusta la ruta si es necesario

// Declaramos el puerto
const PORT = process.env.PORT || 3000;

// Activamos cors
app.use(cors());

// Gestion de JSON entrantes
app.use(express.json());

// Servimos archivos estáticos (HTML, CSS, JS)
app.use(express.static(path.join(__dirname, 'public'))); // Ajusta la ruta si es necesario
app.use(express.static(path.join(__dirname, 'frontend'))); // Ajusta la ruta si es necesario

// Declaración de las rutas principales
app.use('/products', productRoute);
app.use('/auth', authRoutes);

// Inicializamos el servidor
app.listen(PORT, () => {
    console.log(`Servidor escuchando en el puerto: ${PORT}`);
});
