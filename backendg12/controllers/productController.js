// Database import
const db = require("../db/db");

// get all products from db
const getAllProducts = (req, res)=>{
    // create request
    const sql = 'SELECT * FROM products';
    
    // send request to db
    db.query(sql, (err, result)=>{
        if(err){throw err}
        res.json(result);
    });
};

// get product id from bd
const getProductById = (req, res)=>{
    const {id} = req.params;

    const sql = 'SELECT * FROM products WHERE id = ?';

    db.query(sql, [id] ,(err, result)=>{
        if(err){throw err}
        res.json(result);
    });
};

// create product
const createProduct = (req, res)=>{
    const {name, price, category, stock, image} = req.body;

    const sql = 'INSERT INTO products (name,price,category,stock,image) VALUES (?,?,?,?,?)';

    db.query(sql, [name, price, category, stock, image],(err, result)=>{
        if(err){throw err}
        res.json({msj:"Producto agregado."});
    });
};

//update product
const updateProduct = (req, res)=>{
    const {id} = req.params;
    const {name, price, category, stock, image} = req.body;

    const sql = 'UPDATE products SET name = ?, price = ?, category = ?, stock = ?, image = ? WHERE id = ?;'

    db.query(sql, [name, price, category, stock, image, id],(err, result)=>{
        if(err){throw err}
        res.json({msj:"Producto actualizado."});
    });
};

const deleteProduct = (req, res) => {
    const {id} = req.params;
    const sql = 'DELETE FROM products WHERE id = ?';
    
    db.query(sql, [id], (err, result) => {
        if(err){throw err}
        res.json({msj: "Producto eliminado."});
    });
};

module.exports = {
    getAllProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
}
